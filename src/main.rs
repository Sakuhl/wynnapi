#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

#[macro_use]
extern crate diesel;
use diesel::prelude::*;
use diesel::pg::PgConnection;

extern crate wynncraft;

extern crate reqwest;

use std::env;
use std::thread;

mod models;
use models::*;
mod schema;
use schema::*;

#[get("/list")]
fn list() -> String {
	let mut json = "{\"guilds\":[".to_owned();
	for guild in guilds::table.load::<Guild>(&establish_connection()).unwrap() {
		json.push_str("{");
		json.push_str(&format!(r#""name":{:?},"prefix":{:?}"#, guild.name, guild.prefix));
		json.push_str("},");
	}
	json.push_str("]}");
	json
}

#[get("/update-list")]
fn update_list() -> String {
	if let Ok(guild_list) = wynncraft::guild_list() {
		let conn = establish_connection();

		let mut guilds = vec![];

		guild_list.into_iter().for_each(|name| {
			thread::sleep_ms(2500);
			println!("Getting {:?}", name);
			if let Ok(Some(prefix)) = wynncraft::guild(&name).map(|x| x.map(|x| x.prefix)) {
				let guild = Guild { prefix: prefix.trim().to_owned(), name };
				guilds.push(guild);
				let _ = reqwest::get("https://wynnapi.herokuapp.com/").unwrap();
			} else {
				println!("Fail!");
			}
		});
	
		if let Err(e) = diesel::delete(guilds::table).execute(&conn) {
			println!("Mysterious error from DB: {:?}", e);
		}
		println!("Inserting guilds...");
		for guild in &guilds {
			if let Err(e) = diesel::insert_into(guilds::table)
				.values(guild)
				.on_conflict(guilds::prefix)
				.do_nothing()
				.execute(&conn) {
				println!("Mysterious error from DB: {:?}", e);
			}
		}

		println!("Guilds updated!");
	}
	"done".to_owned()
}

fn main() {
	let server_handle = thread::Builder::new()
	    .spawn(|| {
				rocket::ignite().mount("/", routes![list, update_list]).launch()
		})
    	.unwrap();

	server_handle.join().unwrap();
}

fn establish_connection() -> PgConnection {
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}
