use ::schema::*;

#[derive(Queryable, Insertable)]
#[table_name="guilds"]
pub struct Guild {
	pub prefix: String,
	pub name: String
}